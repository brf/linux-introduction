# Linux at a Glance - A Short Introduction Into Linux

This documents accompies the de.NBI course "Linux at a Glance -  a short introduction into Linux and the command line".

[[_TOC_]]

On Linux and Unix, every user has a unique user name. When they log onto the system, they are placed in a home directory, which is a portion of the disk space reserved just for them. When you log onto a Linux system, your main interface to the system is  called Shell or Terminal. This is the program that presents you with the dollar sign (`$`) prompt. This prompt means that the shell is ready to accept your typed commands. It is often preceded by the user name as well as the current directory.

Unix commands are strings of characters typed in at the keyboard. To run a command, you just type it in and press the *Enter* key. We will look at several of the most common commands below.

Commands often have _parameters_, e. g. a file to work on. Theses are typed in after the command and are separated by spaces, e. g. `less textfile.txt` opens the file `textfile.txt` for reading.

In addition, Unix extends the power of commands by using special flags or *switches*. Switches are usually preceded with a dash (`-`), e. g. `ls -lh`.

Beforehand our course we recommend to download our 'cheat sheet' containing useful commands and their parameters [PDF](cheatsheet.pdf).


## Important notes

### Issue: ELIXIR account not linked with de.NBI

- If you have registered for an ELIXIR/de.NBI account and followed the registration procedure, yet your account does not work in the de.NBI cloud portal, there might be an issue with the registration. After initial registration you will get a conformation mail with a link. However, this link is very long and some mail clients have issues showing the correct complete link, hence the registration could have not been finished. If that applies to you, please use the following link and try to finish the registration:

https://perun.elixir-czech.cz/registrar/?vo=elixir&targetnew=https://perun.elixir-czech.cz/registrar/?vo=denbi&targetexisting=https://perun.elixir-czech.cz/registrar/?vo=denbi

### Issue: Empty XFCE panel

- Mostly and per default we will use the XFCE desktop environment (because it is lightwight and fast). After the first initial login to a newly created virtual instance in the de.NBI cloud you will be asked whether you wan an **one empty panel** or **use default config** environment. Please use the **default** panel environment. If you, by chance, selected the empty one, no links will be created upon the first start and you need to search for every application by yourself. However, you can re-initiate this process by following these steps after login to the VM:


```
pkill xfconfd
rm -rf ~/.config/xfce4/panel
rm -rf ~/.config/xfce4/xfconf/xfce-perchannel-xml/
xfce4-panel -r
```



### Opening a terminal window

- If not yet open go to the _Applications_ menu in the upper left corner and select _Terminal Emulator_ to open a new terminal.

![Opening a terminal window](images/screenshot-terminal.png)

Alternatively, you may press the key combination _Alt_ + _F2_ and enter _gnome-terminal_ to start a terminal.

- It is possible to have more than one terminal open at the same time.

- Do not use your HOME-directory to store data but instead create a directory "/mnt/course" and copy any files to this directory. You may create a symlink to this folder from within your HOME-directory.

- If you want to turn off system beeps (you should), type the following in the terminal:

`xset -b `


## Exercise - part 1 

### File System Commands

<u>Tasks:</u>

1. change to your home directory
  - Use the command "cd" for this purpose. You may directly change to the folder by just typing "cd" without any parameters. Alternatively, the full command would be "cd /home/ubuntu" or "cd ~" as an abbreviation.

2. Within this directory create a file named ‘test.txt’
  - Use the command "touch" (afterwards check if the file is there)

3. Create a directory named ‘tutorial’
  - To do this use the command "mkdir" (look into the documentation if necessary)

4. Copy ‘test.txt’ into the directory ‘tutorial’
  - To do this use the command "cp"

5. Delete ‘test.txt’ (in your home)
  - To do this use the command "rm"

6. Change to ‘tutorial’ and rename ‘test.txt’ to ‘file.txt’
  - Use the commands "cd" and "mv" for this purpose

7. Remove the directory ‘tutorial’ and its contents
  - To do this use the command "rmdir" and/or "rm" ("rmdir" can only delete emtpy directories, "rm" together with a specific option also directories including content)

<details><summary>Show solution</summary><pre><code>
cd
<br />
touch test.txt
ls -lrt
<br />
mkdir tutorial
<br />
cp test.txt tutorial/
<br />
rm test.txt
<br />
cd tutorial
mv test.txt file.txt
ls
<br />
rm file.txt
cd .. 
rmdir tutorial
</code></pre></details>


### Links

<u>Tasks:</u>

1. Change to /mnt/
  - To do this use the command "cd"

2. Create a directory with the name 'course' (this requires some additional steps!)
  - Use the command "mkdir" for this purpose
  - This can not be done with normal permissions! Just try it!
  - We need to do this with administrative priviledges as the so called root user. For this purpose please prepend the command "sudo" to your "mkdir" command
  - Secondly, we want to give our normal user read and write access to this directory by simply changing the owner of this directory to our user "ubuntu"
  - For this we need to enter the following command: "sudo chown ubuntu:ubuntu course"

3. Go back to your home directory 

4. Create a soft link called 'workdir' to /mnt/course
  - Use the command "ln" (look into the documentation if necessary). Change into the directory workdir to verify that the link is functional.

<details><summary>Show solution</summary><pre><code>
cd /mnt
<br />
sudo mkdir course
sudo chown ubuntu:ubuntu course
<br />
cd
<br />
ln -s /mnt/course workdir 
</code></pre></details>


## Exercise - part 2

### Display File Content

<u>Tasks:</u>

1. We need to retrieve some additional data for this course. To do this we will use the command "wget" to download files to our computer. 
  - In this task we need a small sample file from the web. The file is located at this location: 
"https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/linuxcourse/seqs.fasta"
  - Download this file using the command "wget"
  
2. Use head and tail to inspect the file

3. Print the first and last entry of the fasta file to the command line
  - An entry in a fasta file always includes a header (beginning with '>') and a sequence in the following line or lines. Have a look at the help-pages of "head" and "tail" to learn about getting a specific number of lines.

4. Browse the file using less, search for start codons
  - Use may scroll in the file using the arrow keys. The 'space' key will skip a complete page, 'b' will scroll one page back. Using the key '/' you may search for an arbitrary text within the file. 'q' will quit.

<details><summary>Show solution</summary><pre><code>
cd ~/workdir
wget https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/linuxcourse/seqs.fasta
<br />
head seqs.fasta
tail seqs.fasta
<br />
head -n 2 seqs.fasta
tail -n 2 seqs.fasta
<br />
less seqs.fasta
(/ATG to search for start codons)
</code></pre></details>

### Wildcards

<u>Tasks:</u>

1. List all tools in /usr/local/bin/ starting with ‘blast’
  - Note: '*' matches zero or more characters

2. List all tools in /usr/local/bin/ starting with ‘blast’ followed by one additional character
  - Note: '?' matches exactly one character

3. List all tools in /usr/local/bin/ starting with ‘a’ or ‘b’ and ending with ‘c’ or ‘d’
  - Note: use square brackets to define a specific set of characters

4. We need some more data for our course. Please again use the command "wget" to download this data to your computer. 
  - The data is located at this location: 
"https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/linuxcourse/linuxdata.tar.gz"
  - Download this file using the command "wget" to your workdir
  - The data is compressed in an archived, which we first need to extract
  - This can be done using the command "tar" with the options "xfz", in full: 
  "tar xfz linuxdata.tar.gz" 
  - Verify that a folder named "linuxdata" is present. You may delete the file "linuxdata.tar.gz" afterwards.

5. Copy all sequence files from the directory "linuxdata" to your workdir (except seqs.fasta!)

<details><summary>Show solution</summary><pre><code>
ls /usr/local/bin/blast*
<br />
ls /usr/local/bin/blast?
<br />
ls /usr/local/bin/[ab]*[cd]
<br />
cd ~/workdir
wget https://openstack.cebitec.uni-bielefeld.de:8080/swift/v1/linuxcourse/linuxdata.tar.gz
tar xfz linuxdata.tar.gz
rm linuxdata.tar.gz
<br />
cp ~/workdir/linuxdata/sequences* ~/workdir/
cp ~/workdir/linuxdata/sequences_?.fasta ~/workdir/
cp ~/workdir/linuxdata/sequences_[1-4].fasta ~/workdir/
cp ~/workdir/linuxdata/sequences_{1..4}.fasta ~/workdir/
</code></pre></details>


## Exercise - part 3

### Useful tools: grep and wc

<u>Tasks:</u>

1. Create a soft link to the Araport11_genes.gff from the previously uncompressed 'linuxdata.tar.gz'-archive into your workdir 
  - The file is comparatively large. In such cases it makes sense to use symlinks to prevent having multiple copies of the same file.

2. Inspect the file using less
  - Look at the composition of the file? What could the individual columns mean?

3. How many lines does the file contain?
  - To do this use the command "wc". There is an option to print only the number of lines.

4. How many entries are there for Chromosome 1?

5. Find all entries related to ‘Auxin’

6. Use the command "grep" to find a file inside the "linuxdata" directory that contains the words "Romeo and Juliet".
  - To do this you need to search recursively. This is an option of the "grep"-command

<details><summary>Show solution</summary><pre><code>
ln -s ~/workdir/linuxdata/Araport11_genes.gff ~/workdir/
<br />
cd ~/workdir
<br />
less Araport11_genes.gff
<br />
wc -l Araport11_genes.gff
<br />
grep -c “^Chr1” Araport11_genes.gff
<br />
grep Auxin Araport11_genes.gff
<br />
grep -R 'Romeo and Juliet' linuxdata/
</code></pre></details>


### Useful tools to use with streams

<u>Tasks:</u>

1. Use "cat" and wildcards to combine all sequence-files into a new file “sequences.fasta”

2. Use "head" and "tail" to get the second sequence from sequences.fasta

3. Use "grep" to store the sequence headers of sequences.fasta in a file

4. Use "grep", "head" and "tail" to store headers 11-20 in a file
  - Instead of "grep" you may also use "cat" and the file from the previous task as input.

5. Append the headers 41-50 to the same (!) file

6. Also store the first 50 headers in a separate file. Do this in one command by using "tee" !

7. Use "grep" and "wc" to find out the number of bases in sequences.fasta
  - Hint: lines containing sequence information do not include the ">"-character.

<details><summary>Show solution</summary><pre><code>
cat sequences_[1-4].fasta > sequences.fasta
<br />
head -n 4 | tail -n 2 sequences.fasta
<br />
grep “>” sequences.fasta > headers.txt
<br />
grep “>” sequences.fasta | head -n 20 | tail -n 10 > headers_2.txt
<br />
grep “>” sequences.fasta | head -n 50 | tail -n 10 >> headers_2.txt
<br />
grep '>' sequences.fasta | head -n 50 | tee headers50.txt | tail -n 10 >> headers_2.txt
<br />
grep -v “>” sequences.fasta | wc
</code></pre></details>


## Exercise - extra

### Working with tabular data

<u>Tasks:</u>

1. How many features (CDS/mRNA/UTR…) are there for each type? 
  - Hint: features are in row 3, sort and uniq might be useful

2. Create the same statistic for each chromosome
  - Hint: cut can select multiple columns 

3. How many genes with a ‘kinase’ annotation are there per chromosome?

<details><summary>Show solution</summary><pre><code>
cut -f 3 Araport11_genes.gff | sort | uniq -c 
<br />
or even better:
<br />
cut -f 3 Araport11_genes.gff | sort | uniq -c | grep -v ‘#’
<br />
cut -f 1,3 Araport11_genes.gff | sort | uniq -c | grep -v '##'
<br />
grep kinase Araport11_genes.gff | cut -f 1,3 | grep gene | cut -f 1 | sort | uniq -c
</code></pre></details>

